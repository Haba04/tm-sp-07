package ru.habibrahmanov.tm.model;

public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}
