package ru.habibrahmanov.tm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TmSp07JwtAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TmSp07JwtAppApplication.class, args);
	}

}
