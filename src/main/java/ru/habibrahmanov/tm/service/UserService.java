package ru.habibrahmanov.tm.service;

import com.sun.istack.Nullable;
import ru.habibrahmanov.tm.model.User;

import java.util.List;

public interface UserService {

    @Nullable
    User register(@Nullable User user);

    @Nullable
    List<User> getAll();

    @Nullable
    User findByUsername(@Nullable String name);

    @Nullable
    User findById(@Nullable Long id);

    void delete(@Nullable Long id);
}
