package ru.habibrahmanov.tm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.habibrahmanov.tm.dto.AdminUserDto;
import ru.habibrahmanov.tm.model.User;
import ru.habibrahmanov.tm.service.UserService;

@RestController
@RequestMapping(value = "/api/v1/admin/")
public class AdminRestController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "users/{id}")
    public ResponseEntity<AdminUserDto> getUserById(@PathVariable(value = "id") Long id) {
        User user = userService.findById(id);
        if (user == null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        AdminUserDto adminUserDto = AdminUserDto.fromUser(user);
        return new ResponseEntity<>(adminUserDto, HttpStatus.OK);
    }
}
