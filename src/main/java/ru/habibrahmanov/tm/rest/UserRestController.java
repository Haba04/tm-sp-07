package ru.habibrahmanov.tm.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.habibrahmanov.tm.dto.UserDto;
import ru.habibrahmanov.tm.model.User;
import ru.habibrahmanov.tm.service.UserService;

@RestController
@RequestMapping(value = "/api/v1/users/")
public class UserRestController {

    @Autowired
    private UserService userService;

    @GetMapping(value = "{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable(value = "id") Long id) {
        User user = userService.findById(id);
        if (user == null) return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        UserDto userDto = UserDto.fromUser(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }
}
