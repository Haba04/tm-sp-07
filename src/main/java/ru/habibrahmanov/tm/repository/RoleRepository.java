package ru.habibrahmanov.tm.repository;

import com.sun.istack.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.habibrahmanov.tm.model.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {
    @NotNull
    Role findByName(@NotNull String name);
}
