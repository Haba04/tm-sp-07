package ru.habibrahmanov.tm.repository;

import com.sun.istack.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.habibrahmanov.tm.model.User;

public interface UserRepository extends JpaRepository<User, Long> {
    @NotNull
    User findByUsername(@NotNull String name);
}
